(function ($) {

$.fn.getStyles = function () {
    return window.getComputedStyle(this.get(0), null);
}

})(jQuery);

(function ($) {

$.fn.swapText = function (attr) {
    return this.each(function () {
        var txt = $(this).text();
        $(this).text($(this).attr(attr)).attr(attr, txt);
    });
};

})(jQuery);

var $D = $(document);
var $W = $(window);

(function ($) {

function outerWidthInc (s, m) {
    return  parseFloat(s.paddingLeft) + parseFloat(s.paddingRight) +
        (m ? parseFloat(s.marginLeft) + parseFloat(s.marginRight) : 0);
}

function outerHeightInc (s, m) {
    return  parseFloat(s.paddingTop) + parseFloat(s.paddingBottom) +
        (m ? parseFloat(s.marginTop) + parseFloat(s.marginBottom) : 0);
}

function getElementSpacing ($element, includeMargin) {
    var style = $element.getStyles();
    return {
        width  : outerWidthInc(style, includeMargin),
        height : outerHeightInc(style, includeMargin),
    };
}

//--------------------------- EventGenerator --------------
function EventGenerator () { }
EventGenerator.prototype.emit = function () {
    var name = Array.prototype.splice.call(arguments, 0, 1)[0];
    if (this.handlers[name]) {
        return this.handlers[name].apply(this.ctx, arguments);
    } else {
        if (this.handlers[name] === undefined && console) {
            if (console.error) console.error("unknown event '" + name + "' emited by", this);
            if (console.trace) console.trace();
        }
        return undefined;
    }
}

//--------------------------- ImagePool -------------------
var ImagePool = (function () {
var p = I.prototype;
function I (size) {
    if (typeof size === "undefined")
        size = 10;
    this.run = 0;
    this.max = size;
    this.queue = [];
};
p.with_image = function (cb, ctx) {
    if (this.run < this.max) {
        this.run++;
        cb.call(ctx, new Image());
    } else {
        this.queue.push([cb, ctx]);
    }
};
p.done = function () {
    if (this.queue.length) {
        var c = this.queue.shift();
        c[0].call(c[1], new Image());
    } else {
        this.run--;
    }
};
return I;
})();

//--------------------------- ImageController -------------
function DefineImageController ( handlers ) {

ImageController.prototype = new EventGenerator();

var p = ImageController.prototype;
p.constructor = ImageController;
p.handlers = $.extend({
        resize:     false,
        load_start: false,
        load_end:   false,
        error:      false,
        need_size:  false,
    },
    handlers
);

var imagePool = new ImagePool(3);

function ImageController (img, bigSrc, ctx) {
    this.ctx    = ctx;
    this.img    = img;
    this.big    = bigSrc;
    this.small  = img.src;
    this.scaled = false
    this.info   = {};
    this.load   = {};
    img.src     = '';

    this._load('small', function (img) {
        this.img.width  = img.width;
        this.img.height = img.height;
        this.emit('resize', this.info['small']);
    }, function () {
        this.emit('error');
    });
}

p._type = function (t) {
    this.type = t;
    this.img.src = this[t];
}

p.handleEvent = function (ev) {
    var img  = ev.target,
        info = this.load[img.src];
    delete this.load[img.src];

    this.emit('load_end', info.p);
    if (!info.p)
        this._type(info.t);

    this.info[info.t] = { width : img.width, height : img.height };

    if (info.on[ev.type])
        info.on[ev.type].call(this, img);

    info.i = null;
    imagePool.done();
}

p._load = function (type, onready, onfail, preload) {
    if (!preload)
        this.emit('load_start');

    var src = this[type];
    if (!this.load[src]) {
        this.load[src] = {
            t  : type,
            p  : preload,
            on : { load : onready, error : onfail },
        };
        imagePool.with_image(function (i) {
            this.load[src].i = i;
            i.addEventListener( 'load', this);
            i.addEventListener('error', this);
            i.src = src;
        }, this);
    } else {
        this.load[src].p  = preload;
        this.load[src].on = { load : onready, error : onfail };
    }
}

p._load_cancel = function () {
    for (var o in this.load) {
        this.load[o].p = true;
        this.load[o].on = {};
    }
    this.emit('load_end', true);
}

p._replace = function (type, scale, extra) {
    var info = this.info[type];
    if (!info) {
        this._load(type,
            function () { this._replace(type, scale, extra) },
            function () { this.emit('error'); }
        );
        return;
    } else {
        this._type(type);
    }
    this.scaled = false;
    if (scale) {
        var scale = this.emit('need_size');
        var factor = [
            info.width / scale.maxWidth,
            info.height / scale.maxHeight,
        ].reduce(function (o, e) { return e > o ? e : o }, 0);

        if (factor > 1) {
            info = $.extend({}, info);
            info.width  = Math.round(info.width  / factor);
            info.height = Math.round(info.height / factor);
            this.scaled = true;
        }
    }
    this.img.width  = info.width;
    this.img.height = info.height;
    this.emit('resize', info, extra);
}

p.is_small  = function () { return this.type === 'small'; }
p.is_scaled = function () { return this.scaled; }

p.preload = function () { if (!this.info.big) this._load('big', null, null, true); }

p.grow   = function (scale, extra) {
    if ( this.is_small() || scale != this.scaled)
        this._replace(  'big', scale, extra);
}
p.shrink = function (scale, extra) {
    if (!this.is_small() || scale != this.scaled)
        this._replace('small', scale, extra);
    if (this.type)
        this._load_cancel();
}

p.cycle  = function (extra) {
    var scale, size;
    if (this.is_small()) {
        scale = true;
        size = 'big';
    } else {
        scale = false;
        size = 'small';
    }
    this._replace(size, scale, extra);
}

return ImageController;
}

//--------------------------- ItemController --------------
function DefineItemController ( handlers ) {

ItemController.prototype = new EventGenerator();

var p = ItemController.prototype;
p.constructor = ItemController;
p.handlers = $.extend({
        next:   false,
        prev:   false,
        focus:  false,
        remove: false,
        resize: false,
    },
    handlers
);
var ImageController = DefineImageController({
    resize:     function (imgSize) { this.set_size({ photo: imgSize }); },
    error:      function () { this.set_size({ }); }, // TODO show "unavailable" placeholder
    load_start: function () { this.item.classList.add('loading'); },
    load_end:   function () { this.item.classList.remove('loading'); },
    need_size:  function () {
        var s = getElementSpacing(this.$container, true);
        return { maxWidth : $W.width() - s.width, maxHeight : $W.height() - s.height };
    }
});

function ItemController (item, options, ctx) {
    var _this = this;
    var $item = $(item);

    this.ctx = ctx;
    this.item = item;
    this.$container = $item.children();
    this.$photo   = this.$container.children('.photo');
    this.$toolbar = this.$container.children('.toolbar');

    this.ready = 0; // number of calls to "set_size" for comlete init
    this.size = {
        minW    : options.columnWidth,
        minH    : options.rowHeight,
        photo   : { width: 0, height: 0 },
        toolbar : { width: 0, height: 0 },
    };

    this.$container.css({
        transitionProperty : 'width, height',
        transitionDuration : options.transitionDuration,
    });
    if (this.$photo.length) {
        ++this.ready;

        var $link  = this.$photo.find('a'),
            $image = this.$photo.find('a > img');
        this.image = new ImageController($image.get(0), $link.attr('href'), this);

        $image.css({
            transitionProperty : 'width, height',
            transitionDuration : options.transitionDuration,
        });
        this.size.photo = { width: this.size.minW, height: this.size.minH };

        var $zoom = $('<div class="zoom"/>'),
            $prev = $('<div class="browser prev"/>'),
            $next = $('<div class="browser next"/>');
        this.$photo.append($prev);
        this.$photo.append($next);
        this.$photo.append($zoom);

        $link.on('click', function (ev) {
            ev.preventDefault();
            document.activeElement.blur();
            _this.emit('focus', _this);
            _this.image.cycle();
        });
        $zoom.on('click', function (ev) {
            ev.preventDefault();
            document.activeElement.blur();
            _this.grow(false);
        });
        $prev.on('click', function (ev) {
            ev.preventDefault();
            document.activeElement.blur();
            if (_this.image.is_small()) _this.grow(true);
            else _this.emit('prev', _this);
        });
        $next.on('click', function (ev) {
            ev.preventDefault();
            document.activeElement.blur();
            if (_this.image.is_small()) _this.grow(true);
            else _this.emit('next', _this);
        });
    }

    if (this.$toolbar.length) {
        ++this.ready;

        this.$fold = this.$toolbar.find('.fold');
        this.$fold_toggle = this.$toolbar.find('a.folder');
        this.$fold.hide();
        this.toolbar = {
            size : {
                visible : this.getToolbarSize(),
                hidden  : this.getToolbarSize(),
            },
            folded : true,
            fold_toggle : function () {
                _this.$fold_toggle.swapText('toggle');

                _this.toolbar.folded = !_this.toolbar.folded;
                _this.$fold.toggle();
                _this.set_size({ toolbar: _this.toolbar.size[_this.toolbar.folded ? 'hidden' : 'visible'] });
            },
        };

        this.$toolbar.fontsLoaded(function () { _this.fontReady() }, false);

        this.$fold_toggle.on('click', function (ev) {
            _this.emit('focus', _this);
            _this.toolbar.fold_toggle()
        });
        this.$toolbar.on('click', function (ev) {
            if (ev.target.tagName == 'A') return;
            _this.$fold_toggle.click()
        });
        this.$toolbar.find('a.ctrl.fav').on('click', function (ev) {
            var curl = window.location.href,
                turl = this.href;
            ev.preventDefault();
            $.get(turl, function () {
                history.replaceState(null, '', turl);
                history.replaceState(null, document.title, curl);
            });
        });
        this.$toolbar.find('a.ctrl.delete').on('click', function (ev) {
            ev.preventDefault();
            var _cb = function () { _this.emit('remove', _this); };
            $.ajax({
                url : this.href,
                statusCode : { 200: _cb, 302: _cb, }
            });
        });
    }
}

p.getToolbarSize = function () {
    return { width: this.$toolbar.width(), height: this.$toolbar.height() };
};

p.fontReady = function () {
    if (!this.$toolbar.length) return;

    if (this.toolbar.folded) this.$fold.show();
    this.toolbar.size.visible = this.getToolbarSize();
    this.$fold.hide();
    this.toolbar.size.hidden  = this.getToolbarSize();
    if (!this.toolbar.folded) this.$fold.show();

    this.set_size({ toolbar: this.toolbar.size[this.toolbar.folded ? 'hidden' : 'visible'] });
};

p.get_size = function (with_toolbar) {
    inc = getElementSpacing(this.$container, false);
    return {
        width:  Math.max(this.size.photo.width, this.size.toolbar.width) + inc.width,
        height: this.size.photo.height + (with_toolbar ? this.size.toolbar.height : 0) + inc.height
    };
};

p.set_size = function (sizes) {
    if (sizes.photo)   this.size.photo   = sizes.photo;
    if (sizes.toolbar) this.size.toolbar = sizes.toolbar;

    if (!sizes.photo)   sizes.photo   = this.size.photo;
    if (!sizes.toolbar) sizes.toolbar = this.size.toolbar;

    var width  = Math.max(this.size.minW, sizes.photo.width, sizes.toolbar.width),
        height = Math.max(this.size.minH, sizes.photo.height) + sizes.toolbar.height,
        inc = getElementSpacing(this.$container, true);

    this.$container.width(width).height(height);
    $(this.item)
        .width(width + inc.width)
        .height(height + inc.height);

    if (this.image.is_small())
        this.item.classList.remove('expanded');
    else
        this.item.classList.add('expanded');

    if (this.image.is_small() || this.image.is_scaled())
        this.item.classList.remove('zoomed');
    else
        this.item.classList.add('zoomed');

    this.emit('resize', this, this.ready-- > 0);
};

p.shrink   = function (scale) {
    this.image.shrink(scale);
    if (!this.toolbar.folded) this.toolbar.fold_toggle();
};
p.grow     = function (scale) { this.emit('focus', this); this.image.grow(scale); };
p.preload  = function () { this.image.preload(); };
p.favorite = function () { this.$toolbar.find('a.ctrl.fav').click(); };

return ItemController;
}

//--------------------------- ExpandableMasonry -----------
ExpandableMasonry.prototype = new EventGenerator();

var p = ExpandableMasonry.prototype;
p.constructor = ExpandableMasonry;
p.options = {
    columnWidth         : 120,
    rowHeight           : 120,
    itemSelector        : '.item',
    transitionDuration  : '0.4s',
    low_watermark       : 1,
    exclusive_zoom      : false,
    layout_style        : 'row',
};
p.handlers = {
    itemchanged: false,
    low_watermark: false,
};

var ElementMatches = (function () {
    var proto = Element.prototype;
    var method = proto.matches && "matches";
    if (!method) {
        for (var i in proto)
            if (i.indexOf('MatchesSelector') > -1) {
                method = i;
                break;
            }
    }
    return method
        ? function (el, sel) { return el[ method ](sel) }
        : function (el, sel) { return true }
})();

var ItemController = DefineItemController({
    resize: function (item, init) {
        if (this.lastChangedItem == item) {
            var next = this.nextItem(item, 1, 'image');
            if (next) next.preload();
        }
        this.layout(!init && this.lastChangedItem == item);
    },
    focus: function (item) { this.focus(item) },
    prev: function (item) { this.browse(item, -1) },
    next: function (item) { this.browse(item,  1) },
    remove: function (item) { this.remove([item]) },
});

p.append = function (items) {
    this.masonry.appended(items);
    for (var i = 0; i < items.length; ++i) {
        if (ElementMatches(items[i], this.options.itemSelector))
            this.items.push(new ItemController(items[i], this.options, this));
    }
};

p.remove = function (items) {
    for (var i = 0; i < items.length; ++i) {
        var j = this.items.indexOf(items[i]);
        if (j < 0) continue;
        this.items.splice(j, 1);
        items[i] = items[i].item;
    }
    this.masonry.remove(items);
    this.masonry.layout();
};

p.nextItem = function (start, step, prop) {
    if (typeof start !== 'number')
        start = this.items.indexOf(start);
    for (var i = start+step; i > -1 && i < this.items.length; i += step)
        if (this.items[i])
            if (this.items[i][prop]) {
                if (this.items.length - (i+step) < this.options.low_watermark)
                    this.emit('low_watermark');
                return this.items[i];
            }
    return undefined;
};

p.focus = function (item) {
    var old = this.lastChangedItem;
    this.lastChangedItem = item;
    if (old && old != item) old.shrink(false);
    this.emit('itemchanged', this.items.indexOf(item));
};

p.expand = function (item, scale) {
    if (typeof item === 'number')
        item = this.items[item];
    if (typeof item === 'undefined')
        return;

    item.grow(scale);
};

p.browse = function (item, shift) {
    var next = this.nextItem(item, shift, 'image');
    if (next) next.grow(true);
};

p.layout = function (scroll) {
    this.layout.scroll = this.layout.scroll || scroll;
    if (!this.layout.timer) {
        var _this = this;
        this.layout.timer = setTimeout(function () {
            delete _this.layout.timer;
            _this.masonry.layout();
            if (_this.layout.scroll) {
                _this.scrollTo();
                _this.layout.scroll = false;
            }
        });
    }
};

p.scrollTo = function (item) {
    if (typeof item === 'undefined')
        item = this.lastChangedItem;
    if (typeof item === 'undefined')
        return;

    var p = this.masonry.getItem(item.item).position,
        pos = { x: p.x, y: p.y },
        size = item.get_size(true);
    pos.y += Math.round($(this.container).offset().top);

    $('html,body').stop();
    if (pos.y > $W.scrollTop() && pos.y + size.height < $W.scrollTop() + $W.height() &&
        pos.x > $W.scrollLeft() && pos.x + size.width < $W.width())
        return;
    $('html,body').animate({
        scrollTop  : pos.y - Math.max(0, ($W.height() - size.height)/2) + 1,
        scrollLeft : pos.x,
    });
};

p.set_layout_style = function (style) {
    if (typeof style === 'undefined')
        style = this.options.layout_style;

    var _this = this;
    if (style == 'masonry') {
        this.masonry._resetLayout           = this.masonry.__proto__._resetLayout;
        this.masonry._getContainerSize      = this.masonry.__proto__._getContainerSize;
        this.masonry._getItemLayoutPosition = this.masonry.__proto__._getItemLayoutPosition;
        this.options.layout_style = 'masonry';
    } else if (style == 'row') {
        this.masonry._resetLayout = function () { this.x = this.y = this.maxY = 0 };
        this.masonry._getContainerSize = function () { return { height: this.maxY } };
        this.masonry._getItemLayoutPosition = function (i) {
            var excl = _this.options.exclusive_zoom && _this.lastChangedItem && i.element == _this.lastChangedItem.item && !_this.lastChangedItem.image.is_small();
            i.getSize(); this.getSize();
            if (this.x !== 0 && this.x + i.size.outerWidth > this.size.innerWidth || excl) {
                this.x = 0;
                this.y = this.maxY;
            }
            var pos = { x: this.x, y: this.y };
            this.maxY = Math.max(this.maxY, this.y + i.size.outerHeight);
            if (excl) this.y = this.maxY;
            else this.x += i.size.outerWidth;
            return pos;
        };
        this.options.layout_style = 'row';
    } else if (style == 'vertical') {
        this.masonry._resetLayout = function () { this.y = 0 };
        this.masonry._getContainerSize = function () { return { height: this.y } };
        this.masonry._getItemLayoutPosition = function (i) {
            i.getSize(); this.getSize();
            var pos = {
                x: (this.size.innerWidth - i.size.outerWidth) * 0.2,
                y: this.y
            };
            this.y += i.size.outerHeight;
            return pos;
        };
        this.options.layout_style = 'vertical';
    }
};

p.check_options = function (opts) {
    var need_reflow = false;
    for (var opt in opts)
        if (typeof this.__proto__.options[opt] !== 'undefined')
            if (this.options[opt] !== opts[opt]) {
                need_reflow = true;
                this.options[opt] = opts[opt];
            }
    if (need_reflow)
        this.layout(true);
};

function ExpandableMasonry (elem, options) {
    this.items = [];
    this.container = elem;

    this.handlers = $.extend({}, this.handlers, options.handlers);
    delete options.handlers;
    this.options  = $.extend({}, this.options,  options);

    var $body = $(this.container);

    // remove items from container
    var $items = $('<div/>').append($body.children());
    // init masonry
    this.masonry = new Masonry(this.container, this.options);
    this.set_layout_style();
    // add items back
    $body.append($items.children());
    this.append($body.children());

    var actions = {
        37 /* Left   */ : function () { this.browse(this.lastChangedItem, -1); },
        39 /* Right  */ : function () { this.browse(this.lastChangedItem,  1); },
        27 /* Esc    */ : function () {
            var cur = this.lastChangedItem;
            if (cur) cur.shrink(false);
        },
        13 /* Enter  */ : function () {
            var cur = this.lastChangedItem;
            if (cur) cur.grow(false);
        },
        83 /* S      */ : function () {
            var cur = this.lastChangedItem;
            if (cur) cur.favorite();
        },
    };

    var _this = this;
    $D.on('keydown', function (ev) {
        var action = actions[ev.keyCode];
        if (action && !(ev.altKey || ev.ctrlKey)) {
            ev.preventDefault();
            action.call(_this);
        }
    });
}

function wrap_class (className, ctor) {
    $.fn[ className ] = function (options) {
        for (var i = 0; i < this.length; ++i) {
            var instance = $.data(this[i], className);
            if (typeof options === 'string') {
                var res = instance[ options ].apply(instance, Array.prototype.slice.call(arguments, 1));
                if (res !== undefined) return res;
            } else {
                if (instance === undefined) {
                    instance = new ctor(this[i], options);
                    $(this[i]).data(className, instance);
                }
            }
        }
        return this;
    }
}

wrap_class('expandableMasonry', ExpandableMasonry);

})(jQuery);

//--------------------------- Settings --------------------
var Settings = (function ($) {
S.prototype.save = function () {
    localStorage.setItem('st', JSON.stringify(this.t));
};
function S () {
    this.t = $.extend({
            autoload: true,
            exclusive_zoom: false,
        },
        JSON.parse(localStorage.getItem('st') || '{}')
    );
};
return S;
})(jQuery);

function prepare_ui () {
    var settings = new Settings();
    var loading = false;

    var $body = $('#body');
    var load_more = function () {
        if (!settings.t.autoload || loading) return;

        var url = $('#navigation a[rel="next"]').attr('href');
        if (!url) return;

        // TODO start animation?
        loading = true;
        $('<div />').load(url + ' #body,#navigation', function (response, status, xhr) {
            history.replaceState(null, document.title, url);

            var $items = $(this).find('#body .item');
            $body.append($items);
            $body.expandableMasonry('append', $items);

            $('#navigation').replaceWith($(this).find('#navigation'));

            loading = false;
        });
    };

    var h_curr = '',
        h_track = $body.hasClass('hashing');
    $body.expandableMasonry($.extend({
        columnWidth: 250,
        low_watermark: 2,
        handlers: {
            low_watermark: load_more,
            itemchanged: h_track
                ? function (n) {
                    window.location.hash = n+1;
                    h_curr = window.location.hash;
                  }
                : null,
        }
    }, settings.t));

    $W.on('scroll', function () {
        if ($W.height() > $D.height() - 1) return;
        if ($W.scrollTop() + $W.height() < $D.height() - 1) return;
        load_more();
    });

    if (h_track) {
        $W.on('hashchange', function () {
            if (window.location.hash != h_curr)
                $body.expandableMasonry('expand', parseInt(window.location.hash.substr(1))-1, true);
        });
        $W.trigger('hashchange');
    }

    // menu setup
    $('#menu .switch').on('click', function (ev) {
        $('#menu').toggleClass('open');
    });
    for (var opt in settings.t) (function (o) {
        var $e = $('#menu #'+o);
        if (settings.t[o])
            $e.addClass('on');
        $e.on('click', function (ev) {
            settings.t[o] = ev.target.classList.toggle('on');
            $body.expandableMasonry('check_options', settings.t);
            settings.save();
        });
    })(opt);

}

$(function () {
    prepare_ui();
});
