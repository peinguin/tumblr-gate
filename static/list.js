$(function ($) {
var $add = $('#add');
$add.on('click', function () {
    var $input = $('<input type="text" />');
    $input.on('keydown', function (ev) {
        if (ev.keyCode == 13) // Enter
            window.location = window.location.pathname.replace(/\/?$/, $input.val().replace(/^(https?:\/\/)?/, '/').replace(/\.tumblr\.com(\/.*)?$/, ''))
    });
    $add.replaceWith($input);
    $input.focus();
});
});
