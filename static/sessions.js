(function ($) {

$.sessions = {
    get: function () {
        return JSON.parse(localStorage.getItem('sessions') || '[]');
    },

    set: function (ss) {
        localStorage.setItem('sessions', JSON.stringify(ss));
    },

    add: function (s) {
        var ss = $.sessions.get();
        if (ss.indexOf(s) < 0)
            ss.push(s);
        $.sessions.set(ss);
    },

    del: function (s) {
        $.sessions.set( $.sessions.get().filter(function (v) { return v != s; }) );
    }
}

})(jQuery);
