package Local::Tumblr;

use 5.010;

use strict;
use warnings;

use base 'Local::Core';

use AnyEvent;
use AnyEvent::HTTP;
use POSIX ();
use Scalar::Util;

use Local::DB;
use Local::Log;
use Local::XML;

sub ceil0 { $_[0] < 0 ? 0 : $_[0] }
sub pages { int( ($_[0] - 1) / $_[1] ) + 1 }

sub new {
    my ($pkg, %args) = @_;

    my $self = $pkg->SUPER::new(%args,
        route => [
            '' => [ \&parking_page ],
            _new => [ \&generate_key ],
            _digest => [ qr'^\w{4,32}$' => dkey => [ \&digest_render ] ],
            qr'^\w{4,32}$' => key => [
                _digest => [
                    qr'^\w{4,32}$' => dkey => [
                        copy => [ qr'^\w+$' => id => [ qr'^\d{1,2}$' => offset => [ \&digest_copy ] ] ],
                        delete => [
                            qr'^\w+$' => id => [ qr'^\d{1,2}$' => offset => [ \&digest_delete ] ],
                            \&digest_delete
                        ],
                        publish => [ \&digest_publish ],
                        \&digest_render
                    ],
                ],
                qr'^.+$' => blog => [
                    qr'^\d+$' => post => [ \&blog_render ],
                    _digest => [ qr'^\d+$' => id => [ qr'^o\d{1,2}$' => offset => [ \&digest_add ] ] ],
                    'delete' => [ \&blog_delete ],
                    \&blog_last
                ],
                \&home_render
            ],
        ]
    );

    $self->{POSTS_PER_PAGE} = 10;
    $self->{CACHE_TIMEOUT} //= 900;

    my $o = $self; Scalar::Util::weaken($o);
    $self->{db} = Local::DB->new(delete $self->{db_path});
    $self->{xml} = Local::XML->new;
    $self->{xml}->load_template('parking', $args{templates}->{parking} || 'parking.xsl');
    $self->{xml}->load_template('list', $args{templates}->{list} || 'blog-list.xsl');
    $self->{xml}->load_template('page', $args{templates}->{page} || 'blog-page.xsl', {
        url             => sub { $o->{url}->{$_[0]} },
        post            => sub { $o->{pager}->{post} },
        pager_url       => sub { sprintf $o->{pager}->{url}, $o->{pager}->{post} + $_[0]*$o->{POSTS_PER_PAGE} },
        pager_cur       => sub { $o->{pager}->{cur} },
        pager_last      => sub { $o->{pager}->{last} },
        format_date     => sub { POSIX::strftime('%b %e, %Y', gmtime $_[0]) },
        filter          => sub { $_[0] =~ s/<script\b.*?<\/script>//is; $_[0] =~ s/<iframe\b.*?<\/iframe>//is; $_[0] },
    });
    $self->{xml}->load_template('digest', $args{templates}->{digest} || 'blog-digest.xsl');

    $self->{cleanup} = AnyEvent->timer(after => 0, interval =>                    86400, cb => sub { $o->cleanup });
    $self->{cache}   = AnyEvent->timer(after => 0, interval => 4*$self->{CACHE_TIMEOUT}, cb => sub { $o->cache_guard });

    $self
}

sub api_call {
    my ($blog, %args) = @_;

    use constant {
        HTTP_Success        => 2,
        HTTP_Redirection    => 3,
        HTTP_ClientError    => 4,
        HTTP_ServerError    => 5,
    };

    my $api_url = sprintf('https://%s.tumblr.com/api/read?type=photo&%s', $blog,
        $args{id}
        ? sprintf 'id=%d', $args{id}
        : sprintf 'start=%d&num=%d', $args{first} || 0, $args{num} || 1
    );
    debug { "api_call: >>> request '$api_url'" };
    http_request GET => $api_url, timeout => $args{timeout} || 30, headers => { 'User-Agent' => 'curl/7.60.0' }, cb => sub {
        my ($data, $headers) = @_;

        debug { "api_call: <<< request '$headers->{URL}' ($headers->{Status} $headers->{Reason})" };
        trace { '=== Response ===', dumper(headers => $headers, data => $data), '=== cut ===' };

        my $category = int($headers->{Status}/100);
        if ($category == HTTP_Success) {
            $args{on_success} and eval { $args{on_success}->($data) };
            if ($@) {
                error { "api_call: answer processing failed: $@" };
                $args{on_failure} and $args{on_failure}->("http_request: answer processing failed");
            }
        } else {
            $args{on_failure} and $args{on_failure}->("http_request: $headers->{Status} $headers->{Reason}", $category == HTTP_ClientError);
        }
        $args{on_end} and $args{on_end}->();
    }
}

sub cleanup {
    my ($self) = @_;
    debug { "cleanup: started" };
    $self->{db}->cleanup;
    delete $self->{cguards};
    $self->cache_guard;
    debug { "cleanup: finished" };
}

sub cache_update {
    my ($self, $blog, $after, $title, $posts);
    ($self, $blog, @_ == 3 ? ($after) : ($title, $posts)) = @_;
    $after //= $self->{CACHE_TIMEOUT};

    $self->{db}->blog_update($blog, $title, $posts) if defined $posts;

    debug { "cache_update: pending '$blog' blog check in $after seconds" };
    my $o = $self; Scalar::Util::weaken($o);
    $self->{cguards}->{$blog} = AnyEvent->timer(after => $after, cb => sub {
        api_call($blog,
            on_success => sub {
                my $r = $o->{xml}->new(\$_[0]);
                my $title = $r->get_attr('/tumblr/tumblelog', 'title') // "";
                my $posts = $r->get_attr('/tumblr/posts', 'total');
                if (defined $posts) {
                    $o->cache_update($blog, $title, $posts);
                } else {
                    $o->{db}->blog_disable($blog);
                    delete $o->{cguards}->{$blog};
                }
            },
            on_failure => sub {
                $o->{db}->blog_disable($blog) if $_[1];
                delete $o->{cguards}->{$blog};
            },
        );
    });
}

sub cache_guard {
    my ($self) = @_;

    for my $b (grep { $_->{enabled} } @{ $self->{db}->blog_list }) {
        if (not $self->{cguards}->{$b->{name}}) {
            $self->cache_update(
                $b->{name},
                ceil0($self->{CACHE_TIMEOUT} + $b->{mtime} - time) || rand($self->{CACHE_TIMEOUT})
            );
        }
    }
}

sub parking_page {
    my ($self, $ctx) = @_;
    $ctx->{response} = [ $ctx->HTTP_OK, $self->{xml}->new('p')->output('parking') ];
}

sub generate_key {
    my ($self, $ctx) = @_;
    $ctx->{response} = [ $ctx->HTTP_Found, $ctx->mkuri($self->{db}->user_add) ];
}

sub home_render {
    my ($self, $ctx) = @_;
    my $xml = $self->{xml}->new('tumblr', { id => $ctx->{key} });

    for my $blog (@{ $self->{db}->user_blog_list($ctx->{key}) }) {
        $xml->add_node('blog', {
            name    => $blog->{name},
            url     => $ctx->mkuri($ctx->{key}, $blog->{name}, $blog->{post}),
            new     => $blog->{posts} - $blog->{seen},
            posts   => $blog->{posts},
            title   => $blog->{title} // 'unavailable',
            enabled => $blog->{enabled} ? 1 : 0,
        });
    }

    for my $digest (@{ $self->{db}->digest_list($ctx->{key}) }) {
        $xml->add_node('digest', {
            issue   => $digest->{issue},
            year    => $digest->{year},
            count   => $digest->{count},
            url     => $ctx->mkuri($ctx->{key}, '_digest', $digest->{digest}),
            public  => $digest->{published},
            share   => $digest->{published}
                        ? $ctx->mkuri('_digest', $digest->{digest})
                        : $ctx->mkuri($ctx->{key}, '_digest', $digest->{digest}, 'publish'),
        });
    }

    trace { "home_render: list_xml", $xml->serialize };
    $ctx->{response} = [ $ctx->HTTP_OK, $xml->output('list') ];
}

sub blog_delete {
    my ($self, $ctx) = @_;

    if (int( $self->{db}->user_blog_delete(@$ctx{qw/key blog/}) )) {
        $ctx->{response} = [ $ctx->HTTP_Found, $ctx->mkuri($ctx->{key}) ];
    }
}

sub blog_last {
    my ($self, $ctx) = @_;

    my ($post) = $self->{db}->user_blog_info(@$ctx{qw/key blog/});
    $ctx->{response} = [ $ctx->HTTP_Found, $ctx->mkuri(@$ctx{qw/key blog/}, $post // 0) ];
}

sub blog_render {
    my ($self, $ctx) = @_;

    my $posts = $self->{db}->blog_info($ctx->{blog}) // 0;
    my $first = $posts - $ctx->{post} - $self->{POSTS_PER_PAGE};

    api_call($ctx->{blog},
        first       => ceil0($first),
        num         => ceil0($self->{POSTS_PER_PAGE} - ceil0(-$first)),
        on_success  => sub {
            my $xml = $self->{xml}->new(\$_[0]);
            my $rposts = $xml->get_attr('/tumblr/posts', 'total');

            if (!defined $rposts) {
                $ctx->{response} = [ $ctx->HTTP_BadGateway, "Bad Gateway" ];
                return;
            }

            $self->cache_update($ctx->{blog}, $xml->get_attr('/tumblr/tumblelog', 'title'), $rposts);

            if ($posts == $rposts) {
                $self->{url} = { home => $ctx->mkuri($ctx->{key}), blog => $ctx->mkuri($ctx->{key}, $ctx->{blog}) };
                $self->{pager} = {
                    post => $ctx->{post},
                    cur  => pages($ctx->{post} + 1, $self->{POSTS_PER_PAGE}),
                    last => pages($posts, $self->{POSTS_PER_PAGE}),
                    url  => $ctx->mkuri($ctx->{key}, $ctx->{blog}, '%d'),
                };

                my ($post, $seen) = $self->{db}->user_blog_info(@$ctx{qw/key blog/});
                $post //= 0;
                $seen //= 0;
                $ctx->{seen} = $ctx->{post} + $self->{POSTS_PER_PAGE};
                $ctx->{seen} = $seen   if $seen   > $ctx->{seen};
                $ctx->{seen} = $rposts if $rposts < $ctx->{seen};
                if ($post != $ctx->{post} or $seen != $ctx->{seen}) {
                    $self->{db}->user_blog_update($ctx->{key}, $ctx->{blog}, $ctx->{post}, $ctx->{seen});
                }

                $ctx->{response} = [ $ctx->HTTP_OK, $xml->output('page') ];
            } else {
                $ctx->{response} = [ $ctx->HTTP_Found, $ctx->{info}->{request_uri} ];
            }
        },
        on_failure  => sub { $ctx->{response} = [ $ctx->HTTP_BadGateway, $_[0] ] },
    );
}

sub digest_copy {
    my ($self, $ctx) = @_;

    $self->{db}->digest_add($ctx->{key}, $ctx->{id}, $ctx->{offset});

    $ctx->{response} = [ $ctx->HTTP_NoContent, '' ];
}

sub digest_add {
    my ($self, $ctx) = @_;

    api_call($ctx->{blog},
        id => $ctx->{id},
        on_success  => sub {
            my $xml = $self->{xml}->new(\$_[0]);

            my $links = $xml->serialize('
                //post/photoset/photo[@offset="'.$ctx->{offset}.'"]/photo-url |
                //post[not(photoset)]/photo-url'
            );

            $self->{db}->digest_add($ctx->{key}, $xml->get_attr('//post', 'reblog-key'), substr($ctx->{offset}, 1), $links);
            $ctx->{response} = [ $ctx->HTTP_NoContent, '' ];
        },
        on_failure  => sub { $ctx->{response} = [ $ctx->HTTP_BadGateway, $_[0] ] },
    );
}

sub digest_delete {
    my ($self, $ctx) = @_;

    my ($key) = $self->{db}->digest_info($ctx->{dkey}) or return;

    if ($ctx->{key} ne $key) {
        $ctx->{response} = [ $ctx->HTTP_Unauthorized, 'Operation not permitted' ];
        return;
    }

    if (int( $self->{db}->digest_delete($ctx->{dkey}, $ctx->{id}, $ctx->{offset}) )) {
        $ctx->{response} = [ $ctx->HTTP_Found, $ctx->mkuri($ctx->{key}, $ctx->{id} ? ('_digest', $ctx->{dkey}) : ()) ];
    }
}

sub digest_publish {
    my ($self, $ctx) = @_;

    my ($key, undef, undef, $published) = $self->{db}->digest_info($ctx->{dkey}) or return;

    if ($ctx->{key} ne $key) {
        $ctx->{response} = [ $ctx->HTTP_Unauthorized, 'Operation not permitted' ];
        return;
    }

    if ($published) {
        $ctx->{response} = [ $ctx->HTTP_BadRequest, 'Already done' ];
        return;
    }

    if ($self->{db}->digest_publish($ctx->{dkey})) {
        $ctx->{response} = [ $ctx->HTTP_Found, $ctx->mkuri($ctx->{key}) ];
    }
}

sub digest_render {
    my ($self, $ctx) = @_;

    my $info = {url => $ctx->mkuri($ctx->{key} ? $ctx->{key} : (), '_digest', $ctx->{dkey})};
    (@$info{qw/key issue year published/}) = $self->{db}->digest_info($ctx->{dkey}) or return;

    $info->{anon}  = $ctx->{key} ? 0 : 1;
    $info->{owner} = $ctx->{key} && $ctx->{key} eq $info->{key} ? 1 : 0;
    $info->{home}  = $ctx->mkuri($ctx->{key}) if $ctx->{key};

    return unless $info->{published} || $info->{owner};

    my $xml = $self->{xml}->new('digest', $info);
    for my $photo (@{ $self->{db}->digest_content($ctx->{dkey}) }) {
        $xml->add_node('photo', { _content => \$photo->{links}, map { $_ => $photo->{$_} } qw/post_id offset mtime/ });
    }

    trace { "digest_render: digest_xml", $xml->serialize };
    $ctx->{response} = [ $ctx->HTTP_OK, $xml->output('digest') ];
}

1;
