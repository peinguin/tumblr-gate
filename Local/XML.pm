package Local::XML;

use strict;
use warnings;

our $uri = "urn:tumblr";

sub new { bless {}, 'Local::XML::Factory' }

package Local::XML::Factory;

use XML::LibXML;
use XML::LibXSLT;

sub load_template {
    my ($self, $name, $file, $func) = @_;
    my $style_doc = XML::LibXML->load_xml(location => $file);
    my $stylesheet = XML::LibXSLT->new->parse_stylesheet($style_doc);

    for my $f (keys %$func) {
        $stylesheet->register_function($uri, $f, $func->{$f});
    }

    $self->{stylesheets}->{$name} = $stylesheet;
}

sub new {
    my ($factory, $node, $attrs) = @_;

    my $self = bless {}, 'Local::XML::Object';

    if (ref $node) {
        $self->{xml} = XML::LibXML->new->load_xml(string => $node, load_ext_dtd => 0);
    } else {
        $self->{xml} = XML::LibXML::Document->new('1.0', 'utf-8');
        $self->{xml}->setDocumentElement(XML::LibXML::Element->new($node // 'xml'));
        $self->add_node(undef, $attrs);
    }

    $self->{xc}  = XML::LibXML::XPathContext->new($self->{xml});
    $self->{stylesheets} = $factory->{stylesheets};

    $self
}

package Local::XML::Object;

use Carp;

sub find {
    my ($self, $path, $context) = @_;

    $self->{xc}->findnodes($path, $context)
}

sub get_attr {
    my ($self, $path, $attr, $context) = @_;

    my @a = map { $_->getAttribute($attr) } $self->{xc}->findnodes($path, $context);

    wantarray ? @a : $a[0]
}

sub get_contents {
    my ($self, $path, $context) = @_;

    my @a = map { $_->textContent } $self->{xc}->findnodes($path, $context);

    wantarray ? @a : $a[0]
}

sub output {
    my ($self, $name) = @_;

    my $stylesheet = $self->{stylesheets}->{$name} or croak 'Undefined style output requested';

    $stylesheet->output_as_bytes( $stylesheet->transform($self->{xml}) )
}

sub add_node {
    my ($self, $node, $attrs, $root) = @_;

    $root //= $self->{xml}->getDocumentElement;

    my $e = defined $node ? $root->addChild(XML::LibXML::Element->new($node)) : $root;
    while ( my ($name, $value) = each %$attrs ) {
        if ($name eq '_content') {
            if (ref $value) {
                $e->appendWellBalancedChunk($$value)
            } else {
                $e->appendText($value)
            }
        } else {
            if (ref $value eq 'HASH') {
                $self->add_node($name, $value, $e)
            } else {
                $e->setAttribute($name, $value)
            }
        }
    }
}

sub serialize {
    my ($self, $path, $context) = @_;

    $path
    ? join "" => map { $_->toString } $self->{xc}->findnodes($path, $context)
    : $self->{xml}->toString
}

1;
