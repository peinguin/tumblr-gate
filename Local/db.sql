pragma foreign_keys = on;

-- blog info
create table blog (
  "name" text primary key,
  "enabled" int default 1,
  "title" text,
  "posts" int,
  "mtime" int
);

-- session key storage
create table session (
  "key" text primary key,
  "mtime" int
);

-- session data
create table s_data (
  "key" text not null references session(key) on update cascade on delete cascade,
  "name" text not null references blog(name) on update cascade on delete cascade,
  "post" int not null,
  "seen" int not null
);
create unique index s_data_uniq on s_data ("key", "name");
create trigger s_data_upd after update on s_data
begin
  select raise(abort, 'change of key/name are prohibited') where new.key != old.key or new.name != old.name;
  update session set mtime = strftime('%s', 'now') where key = old.key;
end;
create trigger s_data_ins before insert on s_data
begin
  insert or ignore into session (key, mtime) values (new.key, strftime('%s', 'now'));
end;
create trigger s_data_del after delete on s_data
begin
  update session set mtime = strftime('%s', 'now') where key = old.key;
end;

-- digest list
create table digest (
  "digest" text primary key,
  "key" text not null references session(key) on update cascade on delete set null,
  "issue" int not null,
  "year" int not null,
  "published" bool not null default 0
);
create index digest_idx on digest ("key");
create trigger digest_ins after insert on digest
begin
  update session set mtime = strftime('%s', 'now') where key = new.key;
end;
create trigger digest_del after delete on digest
begin
  update session set mtime = strftime('%s', 'now') where key = old.key;
end;

-- digest data
create table d_data (
  "digest" text not null references digest(digest) on update cascade on delete cascade,
  "key" text references session(key) on update cascade on delete set null,
  "post_id" text not null,
  "offset" int not null,
  "mtime" int default (strftime('%s', 'now')),
  foreign key(post_id, offset) references photo_links(post_id, offset) deferrable initially deferred
);
create index d_data_idx on d_data ("digest");
create unique index d_data_uniq on d_data ("key", "post_id", "offset");

-- photo links
create table photo_links ("post_id" text not null, "offset" int not null, "links" text not null);
create unique index photo_links_uniq on photo_links ("post_id", "offset");

--------------------------------------------------------------------------------
-- insert into s_data ("key", "name", "page", "posts") values ('v', 'b1', 2, 300);
-- insert into s_data ("key", "name", "page", "posts") values ('v', 'b2', 5, 150);
-- select * from session;
-- select * from s_data;
