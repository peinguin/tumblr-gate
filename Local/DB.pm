package Local::DB;

use 5.010;

use strict;
use warnings;

use DBI;

=head1 SYNOPSIS

  $uniq_user_key = $db->user_add();
  ($title, $posts) = $db->blog_info($name);
  ($name, $page, $posts) = $db->user_blog_list($uniq_user_key);

=cut

sub new {
    my ($class, $file) = @_;

    my $db = DBI->connect("dbi:SQLite:dbname=$file", "", "", {RaiseError => 0, PrintError => 1});
    $db->do("pragma journal_mode = wal");
    $db->do("pragma foreign_keys = on");
    $db->do("pragma synchronous = normal");

    bless \$db, $class
}

sub db { ${$_[0]} }

sub gen_key {
    my ($self, $query) = (shift, shift);

    my $key;
    my $klen = 6;
    my $retries = 3;

    local $self->db->{PrintError} = 0;

    $self->db->begin_work;
    while (not $self->db->do($query, undef,
            ($key = join "" => map { ['a'..'z', 'A'..'Z']->[rand(52)] } 1 .. $klen), @_
        )) { --$retries || ($retries = 3, ++$klen > 32 && die) }
    $self->db->commit;

    $key
}

sub cleanup {
    my ($self) = @_;

    $self->db->begin_work;
    $self->db->do("delete from    blog where   name not in (select distinct   name from s_data)");
    $self->db->do("delete from  digest where digest not in (select distinct digest from d_data) and published = 1");
    $self->db->do("delete from session where    key not in (select distinct    key from s_data) and key not in (select distinct key from digest)");
    $self->db->commit;
}

sub user_add {
    my ($self) = @_;

    $self->gen_key("insert into session (key) values (?)")
}

sub user_blog_list {
    my ($self, $key) = @_;

    $self->db->selectall_arrayref("select s.name, s.post, s.seen, b.enabled, b.title, b.posts from s_data s left outer join blog b on s.name = b.name where s.key = ?", {Slice => {}}, $key);
}

sub user_blog_info {
    my ($self, $key, $name) = @_;

    $self->db->selectrow_array("select post, seen from s_data where key = ? and name = ?", undef, $key, $name);
}

sub user_blog_delete {
    my ($self, $key, $name) = @_;

    $self->db->do("delete from s_data where key = ? and name = ?", undef, $key, $name);
}

sub user_blog_update {
    my ($self, $key, $name, $post, $seen) = @_;

    if (not int(
        $self->db->do("update s_data set post = ?, seen = ? where key = ? and name = ?", undef, $post, $seen, $key, $name)
    )) {
        $self->db->do("insert into s_data (key, name, post, seen) values (?, ?, ?, ?)", undef, $key, $name, $post, $seen);
    }
}

sub blog_list {
    my ($self) = @_;

    $self->db->selectall_arrayref("select name, enabled, title, posts, mtime from blog", {Slice => {}});
}

sub blog_info {
    my ($self, $name) = @_;

    $self->db->selectrow_array("select posts from blog where name = ?", undef, $name);
}

sub blog_update {
    my ($self, $name, $title, $posts) = @_;

    if (not int(
        $self->db->do("update blog set enabled = 1, title = ?, posts = ?, mtime = ? where name = ?", undef, $title, $posts, time, $name)
    )) {
        $self->db->do("insert into blog (name, title, posts, mtime) values (?, ?, ?, ?)", undef, $name, $title, $posts, time);
    }
}

sub blog_disable {
    my ($self, $name) = @_;

    $self->db->do("update blog set enabled = 0 where name = ?", undef, $name)
}

sub digest_list {
    my ($self, $key) = @_;

    $self->db->selectall_arrayref("select digest, issue, year, published, (select count(*) from d_data where d_data.digest = digest.digest) count from digest where key = ?", {Slice => {}}, $key);
}

sub digest_info {
    my ($self, $digest) = @_;

    $self->db->selectrow_array("select key, issue, year, published from digest where digest = ?", undef, $digest);
}

sub digest_publish {
    my ($self, $digest) = @_;

    $self->db->do("update digest set published = 1 where digest = ?", undef, $digest)
}

sub digest_content {
    my ($self, $digest) = @_;

    $self->db->selectall_arrayref("select l.post_id, l.offset, l.links, d.mtime from d_data d, photo_links l where d.post_id = l.post_id and d.offset = l.offset and d.digest = ?", {Slice => {}}, $digest);
}

sub digest_add {
    my ($self, $key, $post_id, $offset, $links) = @_;

    return unless $links or
        $self->db->selectrow_array("select count(*) from photo_links where post_id = ? and offset = ?", undef, $post_id, $offset);

    my $digest
        =  $self->db->selectrow_array("select digest from digest where key = ? and published = 0", undef, $key)
        || $self->gen_key("insert into digest (digest, key, issue, year) values (?, ?, (select coalesce(max(issue)+1, 1) from digest where key = ? and year = strftime('%Y', 'now')), strftime('%Y', 'now'))", $key, $key);

    $self->db->begin_work;
    $self->db->do("insert or ignore into d_data (digest, key, post_id, offset) values (?, ?, ?, ?)", undef, $digest, $key, $post_id, $offset);
    $self->db->do("insert or ignore into photo_links (post_id, offset, links) values (?, ?, ?)", undef, $post_id, $offset, $links) if $links;
    $self->db->commit;
}

sub digest_delete {
    my ($self, $digest, $post_id, $offset) = @_;

    if (defined $post_id && defined $offset) {
        $self->db->do("delete from d_data where digest = ? and post_id = ? and offset = ?", undef, $digest, $post_id, $offset);
    } else {
        $self->db->do("delete from digest where digest = ?", undef, $digest);
    }
}

1;
