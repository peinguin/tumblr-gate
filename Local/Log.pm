package Local::Log;

use strict;
use warnings;

use Carp;
use IO::Handle;
use Time::HiRes;
use Data::Dumper ();

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT = qw(error info debug trace dumper);

my $level_map;
use constant $level_map = {
    TRACE => 3,
    DEBUG => 2,
    INFO  => 1,
    ERROR => 0,
};
my $level2str = { map { lc } reverse %$level_map };

my $log = *STDOUT;
my $log_to = '-';
my $log_level = INFO;
my $s_time = [Time::HiRes::gettimeofday];

sub import {
    my ($pkg, $args) = @_;

    reopen($args->{file}) if $args->{file};
    $log_level = $level_map->{uc $args->{level}} if $args->{level};

    $pkg->export_to_level(1)
}

sub reopen {
    $log_to = $_[0] if @_;
    if ($log_to eq '-') {
        $log = *STDOUT;
    } else {
        $log = undef;
        open $log, '>>', $log_to or croak "error opening log file '$log_to' : $!";
    }
}

sub print_log {
    my $time = sprintf '[%11.3f] %5s ', Time::HiRes::tv_interval($s_time), $level2str->{+shift};
    $log->printflush( join "\n" => (map { $time . $_ } map { split "\n" } @_), "" )
}

sub dumper (@) {
    my %vars = @_;

    Data::Dumper->new([values %vars], [keys %vars])->Sortkeys(1)->Dump
}

sub trace (&) { print_log(TRACE, $_[0]->()) if $log_level >= TRACE }
sub debug (&) { print_log(DEBUG, $_[0]->()) if $log_level >= DEBUG }
sub info  (&) { print_log( INFO, $_[0]->()) if $log_level >=  INFO }
sub error (&) { print_log(ERROR, $_[0]->()) if $log_level >= ERROR }

1;
