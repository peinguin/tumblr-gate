<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:tumblr="urn:tumblr"
    xmlns:math="http://exslt.org/math"
    exclude-result-prefixes="tumblr math"
    >

    <xsl:import href="common.xsl" />

    <xsl:variable name="title">
        <xsl:choose>
            <xsl:when test="number(/digest/@owner)">My</xsl:when>
            <xsl:otherwise>Someone's</xsl:otherwise>
        </xsl:choose>
        <xsl:text> digest &#x2116;</xsl:text>
        <xsl:value-of select="/digest/@issue" />/<xsl:value-of select="/digest/@year" />
    </xsl:variable>

    <xsl:variable name="action">
        <xsl:choose>
            <xsl:when test="number(/digest/@owner)">delete</xsl:when>
            <xsl:otherwise>copy</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <xsl:template mode="header" match="digest">
        <xsl:apply-templates mode="header-photo" select="." />
        <title><xsl:value-of select="$title" /></title>
        <meta property="og:image" content="{/digest/photo[@mtime=math:min((/digest/photo/@mtime))]/photo-url[@max-width=250]/text()}" />
        <meta property="og:title" content="{$title}" />
    </xsl:template>

    <xsl:template mode="content-header" match="digest">
        <header id="menu" class="menu">
            <a class="b switch"><span class="icon" /></a>
            <div class="drawer">
                <xsl:if test="/digest/@home">
                    <a class="b" href="{/digest/@home}" title="Return to blog list">&#x2191;</a>
                </xsl:if>
                <a id="exclusive_zoom" class="b" title="Expanded image occupy whole row">E</a>
            </div>
        </header>
    </xsl:template>

    <xsl:template mode="body" match="digest">
        <h1>
            <xsl:value-of select="$title" />
            <xsl:if test="/digest/@home">
                <xsl:text>&#160;</xsl:text>
                <a class="ctrl xs" href="{/digest/@home}">&#160;&#x2191;&#160;</a>
            </xsl:if>
        </h1>
        <div id="body" class="hashing">
            <xsl:apply-templates select="photo"><xsl:sort select="@mtime" /></xsl:apply-templates>
        </div>
    </xsl:template>

    <xsl:template match="photo">
        <div class="item">
            <div class="item-container">
                <div class="photo">
                    <a href="{photo-url[@max-width=1280]/text()}">
                        <img src="{photo-url[@max-width=250]/text()}" alt="" />
                    </a>
                </div>
                <div class="toolbar">
                    <table class="layout">
                        <tr>
                            <td class="t">
                                <xsl:if test="not(number(/digest/@anon))">
                                    <a class="ctrl {$action}" href="{/digest/@url}/{$action}/{@post_id}/{@offset}">&#x267b;</a>
                                </xsl:if>
                            </td>
                            <td />
                            <td />
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </xsl:template>

</xsl:stylesheet>
