<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tumblr="urn:tumblr" exclude-result-prefixes="tumblr">

    <xsl:import href="common.xsl" />

    <xsl:variable name="js">
        <![CDATA[
        $(function () {
            $.sessions.get().forEach(function (s) {
                $('#sessions > tbody:last').append('<tr><td><a href="'+s+'">'+s+'</a></td><td><a class="ctrl s" name="'+s+'">&#x274c;</a></td></tr>');
            });
            $('#sessions').on('click', function (ev) {
                if (ev.target.classList.contains('ctrl')) {
                    $.sessions.del(ev.target.name);
                    $(ev.target).parent().parent().remove()
                }
            });
        });
        ]]>
    </xsl:variable>

    <xsl:template mode="header" match="/*">
        <script type="text/javascript"><xsl:value-of select="$js" disable-output-escaping="yes" /></script>
        <title>Tumblr Gate</title>
    </xsl:template>

    <xsl:template mode="body" match="/*">
        <h1>Visited sessions</h1>
        <table id="sessions" class="list">
            <thead>
                <tr><th>Session</th><th>&#160;</th></tr>
            </thead>
            <tbody>
                <tr><td><a class="ctrl s" href="_new">new session</a></td><td>&#160;</td></tr>
            </tbody>
        </table>
        <span id="help">
        <p>
            Click "new session" to create your personal session.
        </p>
        <p>
            To add tracking of tumblr blog click on "add blog" link under "Last
            visited tumblrs". You can click on table header to sort by column
            value.
        </p>
        <p>
            On blog view, under each image, there are control bar, which have
            image post number and few buttons:
            <ul>
                <li>"star" - add to own digest</li>
                <li>"home" - link to post on tumblr</li>
                <li>"external link" - shown only if post have external link</li>
                <li>"expand" - expand control bar to show more details about post along with comments</li>
            </ul>
        </p>
        <p>
            Digest is you own collection of favorite images. Can be shared
            after "publishing". This allow you to hide your session id from
            others.
        </p>
        </span>
    </xsl:template>

</xsl:stylesheet>

